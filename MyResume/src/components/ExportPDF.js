import React, { Component } from 'react';

import {
  Text,
  TouchableHighlight,
  View,AsyncStorage
} from 'react-native';

import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { Container, Button } from 'native-base';
import {HeaderComponent} from './common'
import { connect } from 'react-redux';
import { ActionType } from '../reduxCompoents/constants';

class ExportPDF extends Component {
  constructor(props){
    super(props)
    this.state = {
      resumeHtml:'',
      personalInfo:'',
      experianceInfo:'',
      acedamicsInfo:'',
      projectInfo:'',
      skillsInfo:''
    }
  }

  componentWillMount(){
    AsyncStorage.getItem(ActionType.PERSONAL_INFO_KEY).then((value)=>this.setState({
      personalInfo:value
    }))
    AsyncStorage.getItem(ActionType.EXPERIANCE_KEY).then((value)=>this.setState({
      experianceInfo:value
    }))
    AsyncStorage.getItem(ActionType.ACEDAMIC_KEY).then((value)=>this.setState({
      acedamicsInfo:value
    }))
    AsyncStorage.getItem(ActionType.PROJECT_KEY).then((value)=>this.setState({
      projectInfo:value
    }))
    AsyncStorage.getItem(ActionType.SKILL_KEY).then((value)=>this.setState({
      skillsInfo:value
    }))

  }

  returnSkill(info){
    let valhtml=''
    for(var i =0 ; i<info.length ; i++){
      valhtml = valhtml+'<p>'+info[i].skill+'</p>'
    }

    console.log(valhtml)
    return valhtml
  }

  returnExperience(exp){
    let valhtml=''
    console.log(exp)
    for(var i =0 ; i<exp.length ; i++){
      valhtml = valhtml+'<h3>'+exp[i].company+'</h3>'+'<p>'+exp[i].year+'</p>'+'<p>'+exp[i].designation+'</p>'+
      '<p>'+exp[i].roles+'</p>'
    }

    console.log(valhtml)
    return valhtml
  }

  returnAcedamics(acd){
    let valhtml=''
    console.log(acd)
    for(var i =0 ; i<acd.length ; i++){
      valhtml = valhtml+'<h3>'+acd[i].course+'</h3>'+'<p>'+acd[i].year+'</p>'+'<p>'+acd[i].institute+'</p>'+
      '<p>'+acd[i].percentage+'</p>'
    }

    console.log(valhtml)
    return valhtml
  }

  returnProjects(pro){
    let valhtml=''
    console.log(pro)
    for(var i =0 ; i<pro.length ; i++){
      valhtml = valhtml+'<h3>'+pro[i].projectName+'</h3>'+'<p>'+pro[i].link+'</p>'+'<p>'+pro[i].description+'</p>'
    }

    console.log(valhtml)
    return valhtml
  }

  createHTML(){
    let pi = JSON.parse(this.state.personalInfo)
    let si = JSON.parse(this.state.skillsInfo)
    let ei = JSON.parse(this.state.experianceInfo)
    let ai = JSON.parse(this.state.acedamicsInfo)
    let proI= JSON.parse(this.state.projectInfo)


    this.returnExperience(ei)

    let htmlS= '<h1 align="center">RESUME</h1>'+
    '<h2 margin-left="20">Personal Information: </h2>'+
    `<p>${pi.name}</p> <p>${pi.email}</p> <p>${pi.address}</p> <p>${pi.number}</p>`+
    '<h2>Synopsis: </h2>'+`<p>${pi.synopsis}</p><h2>Technical Skills: </h2>${this.returnSkill(si)}`+
    `<h2>Experience: </h2> ${this.returnExperience(ei)}`+`<h2>Acedemics: </h2> ${this.returnAcedamics(ai)}`+
    `<h2>Representative Projects: </h2> ${this.returnProjects(proI)}`
   
    return htmlS
  }

  async createPDF() {
     let htm=this.createHTML()
    console.log(htm)
    let options = {
      html: htm,
      fileName: 'MyResume',
      directory: 'docs',
      height: 800,
      width: 1056,
      padding: 40,
    };

    let file = await RNHTMLtoPDF.convert(options)
    console.log(file.filePath);
    alert(file.filePath)
  }

  render() {
    return (
      <Container>
        <HeaderComponent
          title={'Export PDF'}
          addPress={() => console.log('pdf')}
          btnTitle={''}
          navigation={this.props.navigation}
        />
        <Button block onPress={this.createPDF.bind(this)} style={{justifyContent:'center' , marginTop:10}}>
          <Text>Export PDF Here</Text>
        </Button>

        <Text style={{alignSelf:'center'}}>Please find file in documents folder after export</Text>
      </Container>
    );
  }
}

// const mapStateToProps = (state) =>{
//   const { name, address, email, synopsis, number} = state.personalInfo
//   const {data} = state.acedamicsInfo
//   const {Edata} = state.experianceInfo
//   const {Pdata} = state.projectInfo
//   const {Sdata} = state.skillInfo

//   return {data,Edata,Pdata,Sdata}
// }

export default ExportPDF