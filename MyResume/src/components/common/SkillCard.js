import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native'

class SkillCard extends Component {

    render() {
        const { close, skills, uid } = this.props
        return (
            <View style={Styles.OuterContainerStyle}>

                <View style={Styles.InnerContainerStyle}>
                    <Text style={Styles.textStyle}>{skills}</Text>
                    <TouchableOpacity
                        style={{ alignSelf:'center',justifyContent:'flex-end'}}
                        onPress={close}>
                        <Image source={require('../../assets/close.png')}
                            style={{ height: 10, width: 10 }}
                        />
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const Styles = {
    InnerContainerStyle: {
        flexDirection: 'row',
        justifyContent:'space-between'
    },

    OuterContainerStyle: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        margin: 5,
        padding: 10
    },

    textStyle: {
        fontSize: 16,
        color: 'black',
        marginLeft:10
    }

}

export { SkillCard }

