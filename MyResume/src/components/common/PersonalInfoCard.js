import React , {Component} from 'react';
import {View , Text, TextInput} from 'react-native';
import {connect} from 'react-redux';

class PersonalInfoCard extends Component{
    render(){
        return(
            <View style={{flexDirection:'row',padding:10,alignItems:'center'}}>
            <Text style={{flex:0.5, fontSize:16,color:'black'}}>{this.props.field}</Text>
            <TextInput
                    style={{
                    backgroundColor:'white',textAlignVertical:'top',fontSize:16,flex:0.5,color:'black'}}
                    placeholder={this.props.placeholder}
                    onChangeText={this.props.textChange}
                    value={this.props.text}
                    underlineColorAndroid="white"
                    multiline={this.props.multiline}
                    editable={this.props.editable}
                    autoCorrect={false}

                />
          </View>
        );
    }
}

export {PersonalInfoCard}