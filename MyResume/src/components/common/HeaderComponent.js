import React , {Component} from 'react';
import{TouchableOpacity , Text,Image} from 'react-native'
import {Container,Header,Body,Title,Left,Right} from 'native-base'

class HeaderComponent extends Component{
    render(){
        const {title, addPress, btnTitle,navigation} = this.props
        return(
               <Header>
                    <Left >
                        <TouchableOpacity onPress={()=> navigation.openDrawer()}>
                            <Image source={require('../../assets/menu_ico.png') }
                            style={{height:20,width:20}}/>
                        </TouchableOpacity>
                    </Left>

                    <Body>
                        <Title>{title}</Title>
                    </Body>
                    <Right>
                        <TouchableOpacity onPress={addPress}>
                            <Text style={{color:'black'}}>{btnTitle}</Text>
                        </TouchableOpacity>
                    </Right>
                </Header>
        );
    }
}

export {HeaderComponent}