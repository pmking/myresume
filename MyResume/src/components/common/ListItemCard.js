import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native'

class ListItemCard extends Component {
    render() {
        const { close, year, course, institute, percentage, uid } = this.props
        return (
            <View style={Styles.OuterContainerStyle}>

                <View style={Styles.InnerContainerStyle}>
                    <Text style={Styles.textStyle}>{year}</Text>
                    <Text style={Styles.textStyle}>{course}</Text>
                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end' ,alignSelf:'center',justifyContent:'center'}}
                        onPress={close}>
                        <Image source={require('../../assets/close.png')}
                            style={{ height: 10, width: 10 }}
                        />
                    </TouchableOpacity>
                </View>

                <Text style={Styles.textStyle}>{institute}</Text>
                <Text style={Styles.textStyle}>{percentage}</Text>

            </View>
        );
    }
}

const Styles = {
    InnerContainerStyle: {
        flexDirection: 'row',
    },

    OuterContainerStyle: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        margin: 5,
        padding: 10
    },

    textStyle: {
        flex: 0.5,
        fontSize: 16,
        color: 'black',
    }

}

export { ListItemCard }

