import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import { HeaderComponent, SkillCard } from './common';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import PopupDialog,{scaleAnimation} from 'react-native-popup-dialog'
import {onSkillTextChange, getSkillList, addSkill, removeSkill} from '../reduxCompoents/actions'
import {PersonalInfoCard} from './common'

class Skills extends Component {

    componentWillMount(){
        this.props.getSkillList()
    }

    onAddPress() {
        this.popupDialog.show()
    }

    CancelPress(){
        this.popupDialog.dismiss()
    }

    DonePress(){
        const { skill } = this.props
        this.props.addSkill({ skill })
        this.popupDialog.dismiss()
    }

    onClosePress(uid){
        this.props.removeSkill(uid)
        this.popupDialog.dismiss()
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent
                    title={'Skills'}
                    addPress={this.onAddPress.bind(this)}
                    btnTitle={'ADD'}
                    navigation={this.props.navigation} />

                {this.props.acedamicLoading ? <Spinner color='blue' /> : <View />}

                <FlatList
                    data={this.props.Sdata}
                    renderItem={({ item }) => <SkillCard
                        close={this.onClosePress.bind(this, item.uid)}
                        skills={item.skill}
                        uid={item.uid}
                    />}
                />

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width={0.8}
                    height={0.3}
                    dialogAnimation={scaleAnimation}
                    containerStyle={{ zIndex: 10, elevation: 10 }}

                >
                    <View style={{
                        width: window.width * 80 / 100, height: 50, backgroundColor: '#1C264B', alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Text style={{ fontSize: 18, color: 'white' }}>Technical Skill</Text>
                    </View>

                    <PersonalInfoCard
                        placeholder="Enter Skill"
                        textChange={(text) => this.props.onSkillTextChange({ prop: 'skill', value: text })}
                        text={this.props.skill}
                        editable={true}
                        field="Skill"
                        multiline={true}

                    />

                    <View style={{ flexDirection: 'row', margin: 20, position: 'absolute', bottom: 0, right: 0 }}>
                        <TouchableOpacity
                            onPress={this.CancelPress.bind(this)}
                        >
                            <Text style={{ fontSize: 16, color: '#929495', paddingTop: 5, paddingBottom: 5 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.DonePress.bind(this)}
                        >
                            <Text
                                style={{ marginLeft: 20, fontSize: 16, color: '#1C264B', paddingTop: 5, paddingBottom: 5 }}
                            >Done</Text>
                        </TouchableOpacity>
                    </View>
                </PopupDialog>
            </Container>
        )
    }
}

const mapStateToProps = (state)=>{
   const {skill,Sdata} = state.skillInfo;
   return {skill,Sdata}
}

export default connect(mapStateToProps , {onSkillTextChange, getSkillList, addSkill, removeSkill})(Skills)