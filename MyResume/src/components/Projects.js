import React, { Component } from 'react';
import { Text, View, Dimensions, TouchableOpacity, FlatList } from 'react-native';
import { HeaderComponent, PersonalInfoCard } from './common';
import { Container, Spinner } from 'native-base';
import PopupDialog, { scaleAnimation } from 'react-native-popup-dialog'
import { connect } from 'react-redux';
import { onProjectTextChange, addProjectData, getProjectList, removeProjectValue } from '../reduxCompoents/actions'
import { ListItemCard } from './common';

const window = Dimensions.get('window');

class Project extends Component {
    componentWillMount() {
        this.props.getProjectList()
    }
    onAddPress() {
        this.popupDialog.show()
    }

    CancelPress() {
        this.popupDialog.dismiss()
    }

    DonePress() {
        const {  projectName,year, link, description } = this.props
        this.props.addProjectData({ projectName,year, link, description })
        this.popupDialog.dismiss()
    }

    onClosePress(uid) {
        this.props.removeProjectValue(uid)
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent
                    title={'Project'}
                    addPress={this.onAddPress.bind(this)}
                    btnTitle={'ADD'} 
                    navigation={this.props.navigation}
                    />
                {this.props.projectLoading ? <Spinner color='blue' /> : <View />}

                <FlatList
                    data={this.props.Pdata}
                    renderItem={({ item }) => <ListItemCard
                        close={this.onClosePress.bind(this,item.uid)}
                        year={item.projectName}
                        course={item.year}
                        institute={item.link}
                        percentage={item.description}
                        uid={item.uid}
                    />}
                />

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width={0.8}
                    height={0.6}
                    dialogAnimation={scaleAnimation}
                    containerStyle={{ zIndex: 10, elevation: 10 }}

                >
                    <View style={{
                        width: window.width * 80 / 100, height: 50, backgroundColor: '#1C264B', alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Text style={{ fontSize: 18, color: 'white' }}>Project Details</Text>
                    </View>

                    <PersonalInfoCard
                        placeholder="Enter Project"
                        textChange={(text) => this.props.onProjectTextChange({ prop: 'projectName', value: text })}
                        text={this.props.projectName}
                        editable={true}
                        field="Project"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Enter Link"
                        textChange={(text) => this.props.onProjectTextChange({ prop: 'link', value: text })}
                        text={this.props.link}
                        editable={true}
                        field="Enter Link"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Description"
                        textChange={(text) => this.props.onProjectTextChange({ prop: 'description', value: text })}
                        text={this.props.description}
                        editable={true}
                        field="Description"
                        multiline={false}

                    />

                    <PersonalInfoCard
                        placeholder="Enter Year"
                        textChange={(text) => this.props.onProjectTextChange({ prop: 'year', value: text })}
                        text={this.props.year}
                        editable={true}
                        field="Year"
                        multiline={true}

                    />

                    <View style={{ flexDirection: 'row', margin: 20, position: 'absolute', bottom: 0, right: 0 }}>
                        <TouchableOpacity
                            onPress={this.CancelPress.bind(this)}
                        >
                            <Text style={{ fontSize: 16, color: '#929495', paddingTop: 5, paddingBottom: 5 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.DonePress.bind(this)}
                        >
                            <Text
                                style={{ marginLeft: 20, fontSize: 16, color: '#1C264B', paddingTop: 5, paddingBottom: 5 }}
                            >Done</Text>
                        </TouchableOpacity>
                    </View>
                </PopupDialog>

            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const { projectName,year, link, description, Pdata,projectLoading  } = state.projectInfo
    return { projectName,year, link, description, Pdata, projectLoading }
}

export default connect(mapStateToProps, 
    { onProjectTextChange, addProjectData, getProjectList, removeProjectValue })(Project)