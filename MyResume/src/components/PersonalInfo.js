import React, { Component } from 'react';
import { Text, View, TextInput, ScrollView } from 'react-native';
import { HeaderComponent, PersonalInfoCard } from './common';
import { Container, Spinner } from 'native-base'
import { connect } from 'react-redux';
import { onTextChange, updatePersonalInfo, getPersonalInfo } from '../reduxCompoents/actions'

class PersonalInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            infoArray: [],
            editable: false,
        }
    }

    componentWillMount(){
        this.props.getPersonalInfo()
    }

    toggleEditable() {
        this.setState({
            editable: !this.state.editable
        }, function afterStateChange() {
            
            if (!this.state.editable) {
                const{name,email,number,address,synopsis} = this.props
                console.log('update values to db')
                this.props.updatePersonalInfo({name,address,email,number,synopsis})
            } else {
                console.log('Donot update values to db')

            }
        })
    }

    onEditPress() {
        this.toggleEditable();
    }

    setTitle() {
        if (this.state.editable === true) {
            return 'SAVE'
        }

        return 'EDIT'
    }
    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent
                    title={'Personal Info'}
                    addPress={this.onEditPress.bind(this)}
                    btnTitle={this.setTitle()} 
                    navigation={this.props.navigation}
                    />

                {this.props.loading?<Spinner color='blue'/>:<View/>}


                <PersonalInfoCard
                    placeholder="Enter Name"
                    textChange={(text) => this.props.onTextChange({ prop: 'name', value: text })}
                    text={this.props.name}
                    editable={this.state.editable}
                    field="Name:"
                    multiline={true}

                />

                <PersonalInfoCard
                    placeholder="E-mail"
                    textChange={(text) => this.props.onTextChange({ prop: 'email', value: text })}
                    text={this.props.email}
                    editable={this.state.editable}
                    field="E-mail:"
                    multiline={true}

                />


                <PersonalInfoCard
                    placeholder="Phone Number"
                    textChange={(text) => this.props.onTextChange({ prop: 'number', value: text })}
                    text={this.props.number}
                    editable={this.state.editable}
                    field="Phone Number:"
                    multiline={true}


                />
               
               <PersonalInfoCard
                    placeholder="Address"
                    textChange={(text) => this.props.onTextChange({ prop: 'address', value: text })}
                    text={this.props.address}
                    editable={this.state.editable}
                    field="Address:"
                    multiline={true}


                />

                 <PersonalInfoCard
                    placeholder="Synopsis"
                    textChange={(text) => this.props.onTextChange({ prop: 'synopsis', value: text })}
                    text={this.props.synopsis}
                    editable={this.state.editable}
                    field="Synopsis:"
                    multiline={true}

                />

            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    const { name, address,email,synopsis,number,loading} = state.personalInfo;
    return { name, address,email,synopsis,number,loading };
}

export default connect(mapStateToProps, { onTextChange, updatePersonalInfo, getPersonalInfo})(PersonalInfo);