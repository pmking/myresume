import React, { Component } from 'react';
import { Text, View, Dimensions, TouchableOpacity, FlatList } from 'react-native';
import { HeaderComponent, PersonalInfoCard } from './common';
import { Container, Spinner } from 'native-base';
import PopupDialog, { scaleAnimation } from 'react-native-popup-dialog'
import { connect } from 'react-redux';
import { onExperianceTextChange, addExperianceData, getExperianceList, removeExperianceValue } from '../reduxCompoents/actions'
import { ListItemCard } from './common';

const window = Dimensions.get('window');

class Experiance extends Component {
    componentWillMount() {
        this.props.getExperianceList()
    }
    onAddPress() {
        this.popupDialog.show()
    }

    CancelPress() {
        this.popupDialog.dismiss()
    }

    DonePress() {
        const {  year, designation, company, roles } = this.props
        this.props.addExperianceData({ year, company, designation, roles })
        this.popupDialog.dismiss()
    }

    onClosePress(uid) {
        this.props.removeExperianceValue(uid)
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent
                    title={'Experience'}
                    addPress={this.onAddPress.bind(this)}
                    btnTitle={'ADD'} 
                    navigation={this.props.navigation}
                    />
                {this.props.experianceLoading ? <Spinner color='blue' /> : <View />}

                <FlatList
                    data={this.props.Edata}
                    renderItem={({ item }) => <ListItemCard
                        close={this.onClosePress.bind(this,item.uid)}
                        year={item.year}
                        course={item.company}
                        institute={item.designation}
                        percentage={item.roles}
                        uid={item.uid}
                    />}
                />

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width={0.8}
                    height={0.6}
                    dialogAnimation={scaleAnimation}
                    containerStyle={{ zIndex: 10, elevation: 10 }}

                >
                    <View style={{
                        width: window.width * 80 / 100, height: 50, backgroundColor: '#1C264B', alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Text style={{ fontSize: 18, color: 'white' }}>Experience Details</Text>
                    </View>

                    <PersonalInfoCard
                        placeholder="Enter Company"
                        textChange={(text) => this.props.onExperianceTextChange({ prop: 'company', value: text })}
                        text={this.props.company}
                        editable={true}
                        field="Company"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Enter Year"
                        textChange={(text) => this.props.onExperianceTextChange({ prop: 'year', value: text })}
                        text={this.props.year}
                        editable={true}
                        field="Enter Year"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Designation"
                        textChange={(text) => this.props.onExperianceTextChange({ prop: 'designation', value: text })}
                        text={this.props.designation}
                        editable={true}
                        field="Designation"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Enter Roles"
                        textChange={(text) => this.props.onExperianceTextChange({ prop: 'roles', value: text })}
                        text={this.props.roles}
                        editable={true}
                        field="Roles"
                        multiline={false}

                    />

                    <View style={{ flexDirection: 'row', margin: 20, position: 'absolute', bottom: 0, right: 0 }}>
                        <TouchableOpacity
                            onPress={this.CancelPress.bind(this)}
                        >
                            <Text style={{ fontSize: 16, color: '#929495', paddingTop: 5, paddingBottom: 5 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.DonePress.bind(this)}
                        >
                            <Text
                                style={{ marginLeft: 20, fontSize: 16, color: '#1C264B', paddingTop: 5, paddingBottom: 5 }}
                            >Done</Text>
                        </TouchableOpacity>
                    </View>
                </PopupDialog>

            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const { year, company, designation, roles, Edata,experianceLoading  } = state.experianceInfo
    return { year, company, designation, roles, Edata, experianceLoading }
}

export default connect(mapStateToProps, 
    { onExperianceTextChange, addExperianceData, getExperianceList, removeExperianceValue })(Experiance)