import React, { Component } from 'react';
import { Text, View, Dimensions, TouchableOpacity, FlatList,ScrollView } from 'react-native';
import { HeaderComponent, PersonalInfoCard } from './common';
import { Container, Spinner } from 'native-base';
import PopupDialog, { scaleAnimation } from 'react-native-popup-dialog'
import { connect } from 'react-redux';
import { onAcedamicTextChange, addAcedamicData, getAcedamicList, removeAcedamicValue } from '../reduxCompoents/actions'
import { ListItemCard } from './common';

const window = Dimensions.get('window');

class Acedamics extends Component {
    componentWillMount() {
        this.props.getAcedamicList()
    }
    onAddPress() {
        this.popupDialog.show()
    }

    CancelPress() {
        this.popupDialog.dismiss()
    }

    DonePress() {
        const { year, course, institute, percentage } = this.props
        this.props.addAcedamicData({ year, course, institute, percentage })
        this.popupDialog.dismiss()
    }

    onClosePress(uid) {
        this.props.removeAcedamicValue(uid)
    }

    render() {
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <HeaderComponent
                    title={'Academics'}
                    addPress={this.onAddPress.bind(this)}
                    btnTitle={'ADD'} 
                    navigation={this.props.navigation}
                    />
                {this.props.acedamicLoading ? <Spinner color='blue' /> : <View />}

                <FlatList
                    data={this.props.data}
                    renderItem={({ item }) => <ListItemCard
                        close={this.onClosePress.bind(this,item.uid)}
                        year={item.year}
                        course={item.course}
                        institute={item.institute}
                        percentage={item.percentage+' %'}
                        uid={item.uid}
                    />}
                />

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width={0.8}
                    height={0.6}
                    dialogAnimation={scaleAnimation}
                    containerStyle={{ zIndex: 10, elevation: 10 }}

                >
                    <View style={{
                        width: window.width * 80 / 100, height: 50, backgroundColor: '#1C264B', alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Text style={{ fontSize: 18, color: 'white' }}>Academics Details</Text>
                    </View>

                    <ScrollView>

                    <PersonalInfoCard
                        placeholder="Enter Course"
                        textChange={(text) => this.props.onAcedamicTextChange({ prop: 'course', value: text })}
                        text={this.props.course}
                        editable={true}
                        field="Course"
                        multiline={true}
                    />

                    <PersonalInfoCard
                        placeholder="Passing Year"
                        textChange={(text) => this.props.onAcedamicTextChange({ prop: 'year', value: text })}
                        text={this.props.year}
                        editable={true}
                        field="Passing Year"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Institution"
                        textChange={(text) => this.props.onAcedamicTextChange({ prop: 'institute', value: text })}
                        text={this.props.institute}
                        editable={true}
                        field="institution"
                        multiline={true}

                    />

                    <PersonalInfoCard
                        placeholder="Percentage"
                        textChange={(text) => this.props.onAcedamicTextChange({ prop: 'percentage', value: text })}
                        text={this.props.percentage}
                        editable={true}
                        field="Percentage"
                        multiline={true}

                    />

                    </ScrollView>

                    <View style={{ flexDirection: 'row', margin: 20, position: 'absolute', bottom: 0, right: 0 }}>
                        <TouchableOpacity
                            onPress={this.CancelPress.bind(this)}
                        >
                            <Text style={{ fontSize: 16, color: '#929495', paddingTop: 5, paddingBottom: 5 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={this.DonePress.bind(this)}
                        >
                            <Text
                                style={{ marginLeft: 20, fontSize: 16, color: '#1C264B', paddingTop: 5, paddingBottom: 5 }}
                            >Done</Text>
                        </TouchableOpacity>
                    </View>
                </PopupDialog>

            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const { course, year, institute, percentage, data, acedamicLoading } = state.acedamicsInfo
    console.log(data)
    return { course, year, institute, percentage, data, acedamicLoading }
}

export default connect(mapStateToProps, 
    { onAcedamicTextChange, 
        addAcedamicData, 
        getAcedamicList, 
        removeAcedamicValue })(Acedamics)