import {DrawerNavigator} from 'react-navigation';
import PresonalInfo from '../components/PersonalInfo'
import Skills from '../components/Skills';
import Experiance from '../components/Experiance';
import Acedamics from '../components/Acedamics';
import Projects from '../components/Projects';
import ExportPDF from '../components/ExportPDF';


export default DrawerNavigator({
  PresonalInfo: {
    screen: PresonalInfo
  },
  Skills: {
    screen: Skills
  },
  Experience: {
    screen: Experiance
  },
  Academics:{
    screen:Acedamics
  },
  Projects:{
    screen:Projects
  },
  ExportPDF:{
      screen:ExportPDF
  }
}, {
  drawerWidth: 300
});
