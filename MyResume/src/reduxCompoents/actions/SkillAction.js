import { ActionType } from '../constants';
import firebase from 'firebase';
import {AsyncStorage} from 'react-native'


export const onSkillTextChange = ({ prop, value }) => {
    return ({
        type: ActionType.SKILL_INPUT_CHANGE,
        payload: { prop, value }
    })
}

export const addSkill = ({ skill }) => {
    return (dispatch) => {
        dispatch({
            type: ActionType.SKILL_INFO_UPDATE_SUCCESS,
            payload: true
        })
        if(validate({ skill })){
            firebase.database().ref('/prashant/resume/skillInfo').
            push({ skill }).then(() => {
                dispatch({
                    type: ActionType.SKILL_INFO_UPDATE_SUCCESS,
                    payload: false
                })
            }).catch(() => {
                dispatch({
                    type: ActionType.SKILL_INFO_UPDATE_FAILURE,
                    payload: false
                })
            });
        }else{
            dispatch({
                type: ActionType.SKILL_INFO_UPDATE_FAILURE,
                payload: false
            })
        }
       
    }
}

export const getSkillList = () => {
    return (dispatch) => {
        dispatch({
            type: ActionType.SKILL_INFO_FETCH_SUCCESS,
            payload: { data: [], load: true }
        })
        firebase.database().ref(`/prashant/resume/skillInfo`)
            .on('value', snapShot => {
                const data = _.map(snapShot.val(), (val, uid) => {
                    return { ...val, uid };
                })

                try {
                    AsyncStorage.setItem(ActionType.SKILL_KEY, JSON.stringify(data))
                } catch (error) {
                    // Error saving data
                    console.log('done not')
                }

                dispatch({ type: ActionType.SKILL_INFO_FETCH_SUCCESS, payload: { data, load: false } });
            }, errorObject => {
                dispatch({ type: ActionType.SKILL_INFO_FETCH_FAILURE, payload: { data: [], load: false } });

            });
    };
}

export const removeSkill = (uid) => {
    return (dispatch) => {
        firebase.database().ref(`/prashant/resume/skillInfo/${uid}`).remove().then(
            console.log('deleted')
        ).catch(
            console.log('some error occured')
        )
    }
}


const validate = ( { skill }) =>{
    if(skill===''){
        alert('skill cant be empty')
        return false
    }

    return true;
}



