import { ActionType } from '../constants';
import firebase from 'firebase';
import {AsyncStorage} from 'react-native'


export const onProjectTextChange = ({ prop, value }) => {
    return ({
        type: ActionType.PROJECT_INPUT_CHANGE,
        payload: { prop, value }
    })
}

export const addProjectData = ({ projectName,year, link, description }) => {
    return (dispatch) => {
        dispatch({
            type: ActionType.PROJECT_INFO_UPDATE_SUCCESS,
            payload: true
        })
        if(validate({ projectName,year, link, description })){
        firebase.database().ref('/prashant/resume/ProjectInfo').
            push({ projectName,year, link, description }).then(() => {
                dispatch({
                    type: ActionType.PROJECT_INFO_UPDATE_SUCCESS,
                    payload: false
                })
            }).catch(() => {
                dispatch({
                    type: ActionType.PROJECT_INFO_UPDATE_FAILURE,
                    payload: false
                })
            });
        }else{
            dispatch({
                type: ActionType.PROJECT_INFO_UPDATE_FAILURE,
                payload: false
            })
        }
    }
}

export const getProjectList = () => {
    return (dispatch) => {
        dispatch({
            type: ActionType.PROJECT_INFO_FETCH_SUCCESS,
            payload: { data: [], load: true }
        })
        firebase.database().ref(`/prashant/resume/ProjectInfo`)
            .on('value', snapShot => {
                const data = _.map(snapShot.val(), (val, uid) => {
                    return { ...val, uid };
                })

                try {
                    AsyncStorage.setItem(ActionType.PROJECT_KEY, JSON.stringify(data))
                } catch (error) {
                    // Error saving data
                    console.log('done not')
                }

                dispatch({ type: ActionType.PROJECT_INFO_FETCH_SUCCESS, payload: { data, load: false } });
            }, errorObject => {
                dispatch({ type: ActionType.PROJECT_INFO_FETCH_FAILURE, payload: { data: [], load: false } });

            });
    };
}

export const removeProjectValue = (uid) => {
    return (dispatch) => {
        firebase.database().ref(`/prashant/resume/ProjectInfo/${uid}`).remove().then(
            console.log('deleted')
        ).catch(
            console.log('some error occured')
        )
    }
}

const validate = ({ projectName,year, link, description }) =>{
    if(projectName===''){
        alert('Project Name cant be empty')
        return false
    }else if(year === ''){
        alert('year cant be empty')
        return false
    }else if(link === ''){
        alert('link cant be empty')
        return false
    }else if(description === ''){
        alert('description cant be empty')
        return false
    }

    return true;
}


