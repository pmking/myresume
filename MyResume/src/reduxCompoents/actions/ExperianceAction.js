import { ActionType } from '../constants';
import firebase from 'firebase';
import {AsyncStorage} from 'react-native'


export const onExperianceTextChange = ({ prop, value }) => {
    return ({
        type: ActionType.EXPERIANCE_INPUT_CHANGE,
        payload: { prop, value }
    })
}

export const addExperianceData = ({ year, company, designation, roles }) => {
    return (dispatch) => {
        dispatch({
            type: ActionType.EXPERIANCE_INFO_UPDATE_SUCCESS,
            payload: true
        })
        if(validate({year, company, designation, roles})){
        firebase.database().ref('/prashant/resume/ExperianceInfo').
            push({ year, company, designation, roles }).then(() => {
                dispatch({
                    type: ActionType.EXPERIANCE_INFO_UPDATE_SUCCESS,
                    payload: false
                })
            }).catch(() => {
                dispatch({
                    type: ActionType.EXPERIANCE_INFO_UPDATE_FAILURE,
                    payload: false
                })
            });
        }else{
            dispatch({
                type: ActionType.EXPERIANCE_INFO_UPDATE_FAILURE,
                payload: false
            })
        }
    }
}

export const getExperianceList = () => {
    return (dispatch) => {
        dispatch({
            type: ActionType.EXPERIANCE_INFO_FETCH_SUCCESS,
            payload: { data: [], load: true }
        })
        firebase.database().ref(`/prashant/resume/ExperianceInfo`)
            .on('value', snapShot => {
                const data = _.map(snapShot.val(), (val, uid) => {
                    return { ...val, uid };
                })

                try {
                    AsyncStorage.setItem(ActionType.EXPERIANCE_KEY, JSON.stringify(data))
                } catch (error) {
                    // Error saving data
                    console.log('done not')
                }

                dispatch({ type: ActionType.EXPERIANCE_INFO_FETCH_SUCCESS, payload: { data, load: false } });
            }, errorObject => {
                dispatch({ type: ActionType.EXPERIANCE_INFO_FETCH_FAILURE, payload: { data: [], load: false } });

            });
    };
}

export const removeExperianceValue = (uid) => {
    return (dispatch) => {
        firebase.database().ref(`/prashant/resume/ExperianceInfo/${uid}`).remove().then(
            console.log('deleted')
        ).catch(
            console.log('some error occured')
        )
    }
}

const validate = ({ year, company, designation, roles}) =>{
    if(year===''){
        alert('year cant be empty')
        return false
    }else if(company === ''){
        alert('company cant be empty')
        return false
    }else if(designation === ''){
        alert('designation cant be empty')
        return false
    }else if(roles === ''){
        alert('roles cant be empty')
        return false
    }

    return true;
}


