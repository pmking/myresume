import {ActionType} from '../constants';
import firebase from 'firebase';
import {AsyncStorage} from 'react-native'


export const onTextChange=({prop,value})=>{
    return({
        type:ActionType.INPUT_CHANGE,
        payload:{prop,value}
    })
}

export const updatePersonalInfo = ({name,address,email,number,synopsis})=>{
    return(dispatch)=>{
        dispatch({
            type:ActionType.UPDATING
        })
        if(validate({name,address,email,number,synopsis})){
            firebase.database().ref('/prashant/resume/personalInfo').
            update({name,address,email,number,synopsis}).then(()=>{
                dispatch({
                    type:ActionType.INFO_UPDATE_SUCCESS
                })
            }).catch(()=>{
                dispatch({
                    type:ActionType.INFO_UPDATE_FAILURE
                })
            });
        }else{
            dispatch({
                type:ActionType.INFO_UPDATE_FAILURE
            })
        }
        
    }
}

export const getPersonalInfo = ()=>{
    return(dispatch)=>{
        dispatch({type:ActionType.INFOFETHING})
        firebase.database().ref(`/prashant/resume/personalInfo`)
            .on('value', snapShot => {
                console.log(snapShot.val().address)

                try {
                    AsyncStorage.setItem(ActionType.PERSONAL_INFO_KEY, JSON.stringify(snapShot.val()))
                } catch (error) {
                    // Error saving data
                    console.log('done not')
                }
        
                dispatch({ type: ActionType.INFO_FETCH_SUCCESS, payload: {data:snapShot.val()} });
            }, errorObject => {
                dispatch({ type: ActionType.INFO_FETCH_FAILURE, payload:  {data:''}  });

            });
    };
}

const validate = ({ name,address,email,number,synopsis}) =>{
    if(name===''){
        alert('name cant be empty')
        return false
    }else if(address === ''){
        alert('address cant be empty')
        return false
    }else if(email === ''){
        alert('email cant be empty')
        return false
    }else if(synopsis === ''){
        alert('synopsis cant be empty')
        return false
    }else if(number===''){
        alert('number cant be empty')
        return false
    }

    return true;
}
