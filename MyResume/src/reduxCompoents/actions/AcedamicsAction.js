import { ActionType } from '../constants';
import firebase from 'firebase';
import {AsyncStorage} from 'react-native'

export const onAcedamicTextChange = ({ prop, value }) => {
    return ({
        type: ActionType.ACEDAMIC_INPUT_CHANGE,
        payload: { prop, value }
    })
}

export const addAcedamicData = ({ year, course, institute, percentage }) => {
    return (dispatch) => {
        dispatch({
            type: ActionType.ACEDAMIC_INFO_UPDATE_SUCCESS,
            payload: true
        })
        if(validate({ year, course, institute, percentage })){
            firebase.database().ref('/prashant/resume/AcedamicInfo').
            push({ year, course, institute, percentage }).then(() => {
                dispatch({
                    type: ActionType.ACEDAMIC_INFO_UPDATE_SUCCESS,
                    payload: false
                })
            }).catch(() => {
                dispatch({
                    type: ActionType.ACEDAMIC_INFO_UPDATE_FAILURE,
                    payload: false
                })
            });
        }else{
            dispatch({
                type: ActionType.ACEDAMIC_INFO_UPDATE_FAILURE,
                payload: false
            })
        }
    }
}

export const getAcedamicList = () => {
    return (dispatch) => {
        dispatch({
            type: ActionType.ACEDAMIC_INFO_FETCH_SUCCESS,
            payload: { data: [], load: true }
        })
        firebase.database().ref(`/prashant/resume/AcedamicInfo`)
            .on('value', snapShot => {
                const data = _.map(snapShot.val(), (val, uid) => {
                    return { ...val, uid };
                })

                try {
                    AsyncStorage.setItem(ActionType.ACEDAMIC_KEY, JSON.stringify(data))
                } catch (error) {
                    // Error saving data
                    console.log('done not')
                }


                dispatch({ type: ActionType.ACEDAMIC_INFO_FETCH_SUCCESS, payload: { data, load: false } });
            }, errorObject => {
                dispatch({ type: ActionType.ACEDAMIC_INFO_FETCH_FAILURE, payload: { data: [], load: false } });

            });
    };
}

export const removeAcedamicValue = (uid) => {
    return (dispatch) => {
        firebase.database().ref(`/prashant/resume/AcedamicInfo/${uid}`).remove().then(
            console.log('deleted')
        ).catch(
            console.log('some error occured')
        )
    }
}

const validate = ({ year, course, institute, percentage}) =>{
    if(year===''){
        alert('year cant be empty')
        return false
    }else if(course === ''){
        alert('course cant be empty')
        return false
    }else if(institute === ''){
        alert('institute cant be empty')
        return false
    }else if(percentage === ''){
        alert('percentage cant be empty')
        return false
    }

    return true;
}

