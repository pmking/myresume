import {combineReducers} from 'redux'
import PersonalInfoReducer from './PersonalInfoReducer';
import AcedamicsReducer from './AcedamicsReducer';
import SkillReducer from './SkillReducer';
import ExperianceReducer from './ExperianceReducer';
import ProjectReducer from './ProjectReducer';

export default combineReducers({
    personalInfo:PersonalInfoReducer,
    acedamicsInfo:AcedamicsReducer,
    skillInfo:SkillReducer,
    experianceInfo:ExperianceReducer,
    projectInfo:ProjectReducer
});