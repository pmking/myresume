import { ActionType } from "../constants";

const initial_state = {
    experianceLoading: null,
    company: '',
    year: '', designation: '', roles: '', Edata: []
};

export default (state = initial_state, action) => {
    switch (action.type) {
        case ActionType.EXPERIANCE_INPUT_CHANGE:
            return { ...state, [action.payload.prop]: action.payload.value }

        case ActionType.EXPERIANCE_INFO_UPDATE_SUCCESS:
            return { ...state, experianceLoading: action.payload ,company:'',year:'',designation:'',roles:''}

        case ActionType.EXPERIANCE_INFO_UPDATE_FAILURE:
            return { ...state, experianceLoading: action.payload }

        case ActionType.EXPERIANCE_INFO_FETCH_SUCCESS:
            return { ...state, experianceLoading: action.payload.load, Edata: action.payload.data }

        case ActionType.EXPERIANCE_INFO_FETCH_FAILURE:
            return { ...state, experianceLoading: action.payload.load, Edata: action.payload.data }

        default:
            return state
    }
}
