import { ActionType } from "../constants";

const initial_state = {
    skillLoading: null,
    skill:'', Sdata: []
};

export default (state = initial_state, action) => {
    switch (action.type) {
        case ActionType.SKILL_INPUT_CHANGE:
            return { ...state, [action.payload.prop]: action.payload.value }

        case ActionType.SKILL_INFO_UPDATE_SUCCESS:
            return { ...state, skillLoading: action.payload ,skill:''}

        case ActionType.SKILL_INFO_UPDATE_FAILURE:
            return { ...state, skill: action.payload }

        case ActionType.SKILL_INFO_FETCH_SUCCESS:
            return { ...state, skillLoading: action.payload.load, Sdata: action.payload.data }

        case ActionType.SKILL_INFO_FETCH_FAILURE:
            return { ...state, skillLoading: action.payload.load, Sdata: action.payload.data }

        default:
            return state
    }
}
