import { ActionType } from "../constants";

const initial_state = {
    projectLoading: null,
    projectName: '',
    year: '', link: '', description: '', Pdata: []
};

export default (state = initial_state, action) => {
    switch (action.type) {
        case ActionType.PROJECT_INPUT_CHANGE:
            return { ...state, [action.payload.prop]: action.payload.value }

        case ActionType.PROJECT_INFO_UPDATE_SUCCESS:
            return { ...state, projectLoading: action.payload ,link:'',year:'',description:'',projectName:''}

        case ActionType.PROJECT_INFO_UPDATE_FAILURE:
            return { ...state, projectLoading: action.payload }

        case ActionType.PROJECT_INFO_FETCH_SUCCESS:
            return { ...state, projectLoading: action.payload.load, Pdata: action.payload.data }

        case ActionType.PROJECT_INFO_FETCH_FAILURE:
            return { ...state, projectLoading: action.payload.load, Pdata: action.payload.data }

        default:
            return state
    }
}
