import { ActionType } from "../constants";

const initial_state = {
    loading: null,
    name: '',
    address: '', email: '', synopsis: '', number: ''
};

export default (state = initial_state, action) => {

    switch (action.type) {
        case ActionType.INPUT_CHANGE:
            return { ...state, [action.payload.prop]: action.payload.value }

        case ActionType.UPDATING:
            return { ...state, loading: true }

        case ActionType.INFO_UPDATE_SUCCESS:
            return { ...state, loading: false }
        
        case ActionType.INFO_UPDATE_FAILURE:
            return {...state, loading: false }

        case ActionType.INFO_FETCH_SUCCESS:{
            const {name,address,email,synopsis,number} = action.payload.data
            return {...state, loading:false,name,address,email,synopsis,number}
        }

        case ActionType.INFOFETHING:{
            return {...state, loading:true}
        }

        case ActionType.INFO_FETCH_FAILURE:{
            return {...state, loading:false}
        }

        default:
            return state
    }

}