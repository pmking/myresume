import { ActionType } from "../constants";

const initial_state = {
    acedamicLoading: null,
    course: '',
    year: '', institute: '', percentage: '', data: []
};

export default (state = initial_state, action) => {
    switch (action.type) {
        case ActionType.ACEDAMIC_INPUT_CHANGE:
            return { ...state, [action.payload.prop]: action.payload.value }

        case ActionType.ACEDAMIC_INFO_UPDATE_SUCCESS:
            return { ...state, acedamicLoading: action.payload ,course:'',year:'',institute:'',percentage:''}

        case ActionType.ACEDAMIC_INFO_UPDATE_FAILURE:
            return { ...state, acedamicLoading: action.payload }

        case ActionType.ACEDAMIC_INFO_FETCH_SUCCESS:
            return { ...state, acedamicLoading: action.payload.load, data: action.payload.data }

        case ActionType.ACEDAMIC_INFO_FETCH_FAILURE:
            return { ...state, acedamicLoading: action.payload.load, data: action.payload.data }

        default:
            return state
    }
}
