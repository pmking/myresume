import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {Text} from 'react-native'
import {applyMiddleware, createStore} from 'redux';
import ReduxThunk from 'redux-thunk'
import firebase from 'firebase'
import Routers from './src/navigation/Router'
import reducers from './src/reduxCompoents/reducers';


export default class App extends Component {
   
    componentWillMount(){
      var config = {
        apiKey: "AIzaSyCVAPspiebqNTSEB44RR7zi_WT5MlaPodA",
        authDomain: "myresume-c662d.firebaseapp.com",
        databaseURL: "https://myresume-c662d.firebaseio.com",
        projectId: "myresume-c662d",
        storageBucket: "myresume-c662d.appspot.com",
        messagingSenderId: "758210301972"
      };
      firebase.initializeApp(config);
    }
    render() {
        return (
            <Provider store={store}>
               <Routers/>
            </Provider>
        );
    }
}

const store = createStore(reducers,{},applyMiddleware(ReduxThunk));